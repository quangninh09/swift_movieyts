//
//  TableViewCell.swift
//  MoviesYTS
//
//  Created by tran tu on 12/05/2021.
//

import UIKit

class MovieCellController: UITableViewCell {

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieDate: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var rating: UILabel!
    
    func setVideo(movie: MovieModel) {
        self.movieImage.image = movie.image
        self.movieDate.text = String(format:"%d" ,movie.date!)
        self.movieName.text = movie.name
        self.rating.text = String(format: "%.1f", movie.rating!)
    }
    
}

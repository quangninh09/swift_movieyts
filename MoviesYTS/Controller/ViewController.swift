//
//  ViewController.swift
//  MoviesYTS
//
//  Created by tran tu on 12/05/2021.
//

import UIKit

class ViewController: UIViewController {
    var originalMoviesList: [MovieModel]?
    var moviesList: [MovieModel] = []
    var queryMovieDataController = QueryMovieDataController()
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        queryMovieDataController.delegate = self
        //query data from yts.com
        queryMovieDataController.fetchData(limit:20)
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
   
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = moviesList[indexPath.row]
        let cellMovie =  tableView.dequeueReusableCell(withIdentifier: "cell") as! MovieCellController
        cellMovie.setVideo(movie: movie)
        return cellMovie
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesList.count
    }
    
}

//MARK: - QueryJSONData
extension ViewController: QueryJSONDataDelegate {
    
    func didFailWithError(_ error: Error) {
        print(error)
    }
    
    func didUploadList(moviesList: [MovieModel]) {
        self.moviesList = moviesList
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension ViewController: UITextFieldDelegate {
    @IBAction func searchPressed(_ sender: UIButton) {
        let searchText = searchTextField.text!
        if searchText == "" {
            moviesList = originalMoviesList!
            tableView.reloadData()
            return
        }
        filterResult(searchText: searchText)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        print(searchTextField.text!)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            moviesList = originalMoviesList!
            tableView.reloadData()
        }
    }
    
    func filterResult(searchText: String) {
        let originalMoviesListTemp = moviesList
        originalMoviesList = originalMoviesListTemp
        for index in stride(from: (moviesList.count - 1), to: 0, by: -1) {
            let tempMovie = moviesList[index]
            if !(tempMovie.name?.contains(searchText) ?? false) {
                moviesList.remove(at: index)
            }
        }
        
        tableView.reloadData()
    }
}

//
//  QueryMovieDataController.swift
//  MoviesYTS
//
//  Created by tran tu on 12/05/2021.
//

import UIKit

protocol QueryJSONDataDelegate {
    func didUploadList(moviesList: [MovieModel])
    func didFailWithError(_ error: Error)
}

struct QueryMovieDataController {
    var delegate: QueryJSONDataDelegate?
    let movieUrl = "https://yts.mx/api/v2/list_movies.json?"
    
    func fetchData(limit: Int) {
        let urlString = "\(movieUrl)limit=\(limit)"
        perfromRequest(urlString)
    }
    
    func perfromRequest(_ urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                delegate?.didFailWithError(error!)
                return
            }
            
            if let safeData = data {
                let moviesList = parseJSON(safeData)
//                delegate?.didUploadList(moviesList: moviesList)
                loadImage(moviesList: moviesList)
            }
            
        }
        
        task.resume()
    }
    
    func parseJSON(_ jsonString: Data) -> [MovieModel] {
        var moviesList: [MovieModel] = []
        let decoder = JSONDecoder()
        do {
            //decode object JSONMovieModel from json response data
            let decodedData = try decoder.decode(JSONMovieModel.self, from: jsonString)
            //get list of movies data
            let moviesArray = decodedData.data.movies
            //add one by one movie's data to a list
            for movie in moviesArray {
                let title = movie.title
                let date = movie.year
                let rating = movie.rating
                let imageUrl = movie.small_cover_image
                let movieModel = MovieModel(name: title, date: date, rating: rating, imageUrl: imageUrl, image: nil)
                moviesList.append(movieModel)
            }
            
        } catch {
            delegate?.didFailWithError(error)
        }
        return moviesList
    }
    
    func loadImage(moviesList: [MovieModel]) {
        var tempMoviesList = moviesList
        for index in 0..<moviesList.count {
            let movie = moviesList[index]
            if let url = URL(string: movie.imageUrl!) {
                let imageSession = URLSession(configuration: .default)
                let loadImage = imageSession.dataTask(with: url) { (data, response, error) in
                    if error != nil {
                        delegate?.didFailWithError(error!)
                        return
                    }
                    
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        tempMoviesList[index].image = image
                        if index + 1 == moviesList.count {
                            delegate?.didUploadList(moviesList: tempMoviesList)
                        }
                    }
                }
                loadImage.resume()
            }
        }
    }
}

//
//  JSONMovieModel.swift
//  MoviesYTS
//
//  Created by tran tu on 12/05/2021.
//

import Foundation

struct JSONMovieModel: Decodable {
    let data: Movies
}

struct Movies: Decodable {
    let movies: [Movie]
}

struct Movie: Decodable {
    let title: String
    let year: Int
    let rating: Double
    let small_cover_image: String
}

//
//  MovieModel.swift
//  MoviesYTS
//
//  Created by tran tu on 12/05/2021.
//

import UIKit

struct MovieModel {
    let name: String?
    let date: Int?
    let rating: Double?
    let imageUrl: String?
    var image: UIImage?
}


